FROM php:7.1.8-apache

MAINTAINER Cristian Thompson

RUN a2enmod rewrite
RUN docker-php-ext-install mysqli